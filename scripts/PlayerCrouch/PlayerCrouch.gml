// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerCrouch(){
	if on_ground == true{
		if key_crouch == 1{
			isCrouch = true;
			hsp = 0;
			isAttack = false;
		}else{
			isCrouch = false;
		}
	}else{
		isCrouch = false;	
	}
}