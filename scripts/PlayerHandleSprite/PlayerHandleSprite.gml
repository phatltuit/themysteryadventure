// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerHandleSprite(){	
	if on_ground == false{	
		if isAttack == true{
			sprite_index = sprAttack;
		}else if isWallSlide == true{
			if vsp != 0{
				sprite_index = sprWallSlide;
			}else{
				sprite_index = sprLedgeGrab;
			}
		}else{
			if vsp < 0 {
				sprite_index = sprJump[0];	
			}else if vsp >= -1 and isRoll == false{	
				if vsp == 0 or sprite_index != sprJump[1]{
					sprite_index = sprJump[1];	
				}
				if image_index + image_speed >= image_number{
					isRoll = true;	
				}
			}else{
				sprite_index = sprJump[2];					
			}	
		}
	}else{
		isRoll = false;
		if isCrouch == true{
			sprite_index = sprCrouch;
		}else if isAttack == true{
			sprite_index = sprAttack;
		}else{
			if isSlide == true{
				sprite_index = sprSlide;
			}else{
				if hsp != 0{				
					sprite_index = sprRun;	
				}else{			
					sprite_index = sprIdle;
				}
			}
		}
	}

}