// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSlide(){
	if sprite_index != sprSlide{
		isSlide = false;
	}
	
	if key_slide == 1 and on_ground == true and isSlide == false{
		image_index = 0;
		isSlide = true;	
	}
	if isSlide == true{
		isAttack = false;
		hsp = moveSpd * 3/2 * image_xscale;	
		if image_index + image_speed >= image_number{
			hsp = 0;
			isSlide = false;
		}
	}	
}