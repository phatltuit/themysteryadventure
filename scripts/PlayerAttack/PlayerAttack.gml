// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerAttack(){
	if on_ground == true{
		if sprite_index == sprJumpAtk{
			isAttack = false;
			sword.sprite_index = noone;
		}		
		
		if key_attack == 1 and isAttack == false and isSlide == false and isCrouch == false{
			attackState = 0;
			sprAttack = sprAtk[0];	
			image_speed = atkSpd;		
			isAttack = true;
			image_index = 0;
			sword.sprite_index = swordMask[attackState];
		}
	
		if isAttack == true{
			hsp = 0;
			if key_attack == 1 and canCombo == true and attackState!= 2{
				attackState++;		
				if attackState > 2{
					attackState = 2;	
				}
				image_index = 0;
				sprAttack = sprAtk[attackState];
				sword.sprite_index = swordMask[attackState];
			}
			if image_index + image_speed>= image_number-2 and image_index + image_speed <= image_number{
				canCombo = true;
			}else if image_index + image_speed > image_number{
				isAttack = false;	
				image_speed = 1;
				canCombo = false;
				sword.sprite_index = noone;
			}else{
				canCombo = false;
			}
		}
	}else{		
		if key_attack == 1 and isAttack == false and isWallSlide == false{	
			sprAttack = sprJumpAtk;
			image_speed = jumpAtkSpd;
			image_index = 0;
			isAttack = true;
			sword.sprite_index = swordMask[3];
		}
		
		if image_index + image_speed >= image_number{
			isAttack = false;	
			image_speed = 1;
			sword.sprite_index = noone;
		}	
	}
}