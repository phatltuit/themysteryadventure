// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerJumpAndGravity(){
	if key_jump == 1 and isCrouch == false and (on_ground == true or sprite_index == sprLedgeGrab){
		isAttack = false;
		isWallSlide = false;
		if sprite_index == sprLedgeGrab{
			vsp = -wallJumpSpd;	
		}else{
			vsp = -jumpSpd;
		}
		minJump = true;
		on_ground = false;
	}

	if !keyboard_check_direct(ord("K")) and vsp < 0 and on_ground == false and minJump == true{
		vsp = -0.25;
		minJump = false;
	}

	if !tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom + 1) != 0 and !tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom + 1) != 0{
		on_ground = false;
	}
	
	
	//Gravity
	if on_ground == true{
		vsp = 0;
	}else{
		vsp += grav;
	}
}