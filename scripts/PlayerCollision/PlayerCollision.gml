// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerCollision(){
	//Mask Collision
	if isCrouch == true{
		mask_index = spr_traveler_crouch_collision_mask;	
	}else{
		mask_index = spr_traveler_stand_collision_mask;	
	}
	
	
	
	//Horizone Collision
	if hsp > 0{
		bbox_side = bbox_right
	}else {
		bbox_side = bbox_left;
	}
	if tilemap_get_at_pixel(tilemap, bbox_side + hsp, bbox_bottom) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_side + hsp, bbox_top) != 0
	or PlayerBBoxCollision() == true{
		if hsp > 0 {		
			x = ceil(x - (x mod 16) + 15 - (bbox_right - x));
			hsp = 0;	
			if vsp >= 0 and on_ground == false{
				if tilemap_get_at_pixel(tilemap, bbox_side + 1, ceil(bbox_top) - 1) != 0 and  tilemap_get_at_pixel(tilemap, bbox_side + 1, ceil(bbox_top) - 2) == 0{
					vsp = 0;
				}else{
					vsp = wallSpd;
				}
				isWallSlide = true;
			}
		}else if hsp < 0{
			x = ceil(x - (x mod 16) - (bbox_left - x));
			hsp = 0;
			if vsp >= 0 and on_ground == false{
				if tilemap_get_at_pixel(tilemap, bbox_side - 1, ceil(bbox_top) - 1) != 0 and  tilemap_get_at_pixel(tilemap, bbox_side - 1, ceil(bbox_top) - 2) == 0{
					vsp = 0;
				}else{
					vsp = wallSpd;
				}
				isWallSlide = true;
			}
		}
	}else{
		isWallSlide = false;	
	}	
	

	//Vertical Collision
	if vsp >= 0{
		bbox_side = bbox_bottom;
	}else{
		bbox_side = bbox_top;
	}

	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_side + vsp) != 0 or tilemap_get_at_pixel(tilemap, bbox_left, bbox_side + vsp) != 0{
		vsp = 0
		if vsp >= 0 {
			y = ceil(y - (y mod 16) + 15 - (bbox_bottom - y));
			on_ground = true;
		}else if vsp < 0{
			y = y - (y mod 16) - (bbox_top - y);
		}
		;	
	}
}