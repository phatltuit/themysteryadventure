/// @description Insert description here
// You can write your code in this editor
event_inherited();

//Animation speed
image_speed = 1;

//PlayerState
on_ground = false;
minJump = true;
isAttack = false;
canCombo = false;
isSlide = false;
isRoll = false;
isWallSlide = false;
isCrouch = false;

//Player Physics
jumpSpd = 5.25;
grav = 0.25;
moveSpd = 1.75;
hsp = 0;
vsp = 0;
atkSpd = 1;
jumpAtkSpd = 0.8;
wallSpd = 0.8;
wallJumpSpd = 4.75;

//Player Collision
tilemap = layer_tilemap_get_id("Collision");
bbox_side = bbox_bottom;


//Player Sprite
sprIdle = spr_traveler_idle;
sprRun = spr_traveler_run;
sprSlide = spr_traveler_slide;
sprCrouch = spr_traveler_crouch;
sprWallSlide = spr_traveler_wall_slide;
sprLedgeGrab = spr_traveler_ledge_grab;
sprJumpAtk = spr_traveler_jump_attack;
sprJump[0] = spr_traveler_jump_0;
sprJump[1] = spr_traveler_jump_1;
sprJump[2] = spr_traveler_jump_2;

sprAtk[0] = spr_traveler_attack_0;
sprAtk[1] = spr_traveler_attack_1;
sprAtk[2] = spr_traveler_attack_2;
sprAttack = sprAtk[0];
attackState = 0;

//Player weapon
sword = instance_create_depth(x,y,10000,objPlayerSword);
swordMask[0] = spr_traveler_attack_0_mask;
swordMask[1] = spr_traveler_attack_1_mask;
swordMask[2] = spr_traveler_attack_2_mask;
swordMask[3] = spr_traveler_jump_attack_mask;

