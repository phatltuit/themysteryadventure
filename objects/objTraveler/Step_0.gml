//Input
key_left_release = keyboard_check_released(ord("A"));
key_right_release = keyboard_check_released(ord("D"));
key_left = keyboard_check(ord("A"));
key_right = keyboard_check(ord("D"));
key_jump = keyboard_check_pressed(ord("K"));
key_attack = keyboard_check(ord("J"));
key_slide = keyboard_check_pressed(ord("L"));
key_crouch = keyboard_check(ord("S"));

//Move on Left/Right
PlayerRun();

//Attack
PlayerAttack();

//Jump
PlayerJumpAndGravity();

//Roll
PlayerSlide();

//Crouch
PlayerCrouch();

//Collision
PlayerCollision();

//Sprite
PlayerHandleSprite();

//Move object
x += hsp;
y += vsp;



